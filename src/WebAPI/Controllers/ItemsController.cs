﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItem;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItemBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Response;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/item")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly ICatalogItemService _itemService;
        public ItemsController(ICatalogItemService itemService)
        {
            _itemService = itemService;
        }
        // GET: api/<ItemsController>
        [HttpGet]
        public async Task<DataResponse<DefaultGetAllData<CatalogItemBO>>> Get([FromQuery] GetItemRequest? request)
        {
            var result = await _itemService.List(request);
            if (result != null)
            {
                return new DataResponse<DefaultGetAllData<CatalogItemBO>> { Code = 200, Message = "Success", Result = result };
            }
            else
            {
                return new DataResponse<DefaultGetAllData<CatalogItemBO>> { Code = 404, Message = "Not Found" };
            }
        }

        // GET api/<ItemsController>/5
        [HttpGet("{id}")]
        public async Task<DataResponse<CatalogItemBO>> Get(int id)
        {
            var result = await _itemService.GetById(id);
            if (result != null)
            {
                return new DataResponse<CatalogItemBO> { Code = 200, Message = "Success", Result = result };
            }
            else
            {
                return new DataResponse<CatalogItemBO> { Code = 200, Message = "Success", Result = result };
            }
        }

        // POST api/<ItemsController>
        [HttpPost]
        public async Task<BaseResopnse> Post([FromBody] CreateUpdateCatalogItemRequest request)
        {
            var result = await _itemService.Insert(request);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }

        // PUT api/<ItemsController>/5
        [HttpPut("{id}")]
        public async Task<BaseResopnse> Put(int id, [FromBody] CreateUpdateCatalogItemRequest request)
        {
            var result = await _itemService.Update(id, request);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }

        // DELETE api/<ItemsController>/5
        [HttpDelete("{id}")]
        public async Task<BaseResopnse> Delete(int id)
        {
            var result = await _itemService.Delete(id);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }
    }
}
