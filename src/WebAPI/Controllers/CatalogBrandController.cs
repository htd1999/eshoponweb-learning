﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogBrandBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Response;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/catalog/brand")]
    [ApiController]
    public class CatalogBrandController : ControllerBase
    {
        private readonly ICatalogBrandService _brandService;
        public CatalogBrandController(ICatalogBrandService brandService)
        {
            _brandService = brandService;
        }
        // GET: api/<CatalogBrandController>
        [HttpGet]
        public async Task<DataResponse<DefaultGetAllData<CatalogBrandBO>>> Get([FromQuery] GetBrandRequest request)
        {
            var result = await _brandService.List(request);
            if (result != null)
            {
                return new DataResponse<DefaultGetAllData<CatalogBrandBO>> { Code = 200, Message = "Success", Result = result };
            }
            else
            {
                return new DataResponse<DefaultGetAllData<CatalogBrandBO>> { Code = 404, Message = "Not Found" };
            }
        }

        // GET api/<CatalogBrandController>/5
        [HttpGet("{id}")]
        public async Task<DataResponse<CatalogBrandBO>> Get(int id)
        {
            var result = await _brandService.GetById(id);
            if (result != null)
            {
                return new DataResponse<CatalogBrandBO> { Code = 200, Message = "Success", Result = result };
            }
            else
            {
                return new DataResponse<CatalogBrandBO> { Code = 200, Message = "Success", Result = result };
            }
        }

        // POST api/<CatalogBrandController>
        [HttpPost]
        public async Task<BaseResopnse> Post([FromBody] string request)
        {
            var result = await _brandService.Insert(request);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }

        // PUT api/<CatalogBrandController>/5
        [HttpPut("{id}")]
        public async Task<BaseResopnse> Put(int id, [FromBody] string request)
        {
            var result = await _brandService.Update(id, request);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }

        // DELETE api/<CatalogBrandController>/5
        [HttpDelete("{id}")]
        public async Task<BaseResopnse> Delete(int id)
        {
            var result = await _brandService.Delete(id);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }
    }
}
