﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Response;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Authorize]
    [Route("api/catalog/type")]
    [ApiController]
    public class CatalogTypeController : ControllerBase
    {
        private readonly ICatalogTypeService _typeService;
        public CatalogTypeController(ICatalogTypeService typeService)
        {
            _typeService = typeService;
        }
        // GET: api/<CatalogTypeController>
        [HttpGet]
        public async Task<DataResponse<DefaultGetAllData<CataLogTypeBO>>> Get([FromQuery] GetTypeRequest request)
        {
            var result = await _typeService.List(request);
            if (result != null)
            {
                return new DataResponse<DefaultGetAllData<CataLogTypeBO>> { Code = 200, Message = "Success", Result = result };
            }
            else
            {
                return new DataResponse<DefaultGetAllData<CataLogTypeBO>> { Code = 404, Message = "Not Found" };
            }
        }

        // GET api/<CatalogTypeController>/5
        [HttpGet("{id}")]
        public async Task<DataResponse<CataLogTypeBO>> Get(int id)
        {
            var result = await _typeService.GetById(id);
            if (result != null)
            {
                return new DataResponse<CataLogTypeBO> { Code = 200, Message = "Success", Result = result };
            }
            else
            {
                return new DataResponse<CataLogTypeBO> { Code = 200, Message = "Success", Result = result };
            }
        }

        // POST api/<CatalogTypeController>
        [HttpPost]
        public async Task<BaseResopnse> Post([FromBody] string request)
        {
            var result = await _typeService.Insert(request);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }

        // PUT api/<CatalogTypeController>/5
        [HttpPut("{id}")]
        public async Task<BaseResopnse> Put(int id, [FromBody] string request)
        {
            var result = await _typeService.Update(id, request);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }

        // DELETE api/<CatalogTypeController>/5
        [HttpDelete("{id}")]
        public async Task<BaseResopnse> Delete(int id)
        {
            var result = await _typeService.Delete(id);
            if (result)
            {
                return new BaseResopnse { Code = 200, Message = "Success" };
            }
            else
            {
                return new BaseResopnse { Code = 404, Message = "Error" };
            }
        }
    }
}
