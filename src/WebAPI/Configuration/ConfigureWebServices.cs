﻿using MediatR;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using Microsoft.eShopWeb.ApplicationCore.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Microsoft.eShopWeb.WebAPI.Configuration
{
    public static class ConfigureWebServices
    {
        public static IServiceCollection AddWebServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ICatalogBrandService, CatalogBrandService>();
            services.AddTransient<ICatalogItemService, CatalogItemService>();
            services.AddTransient<ICatalogTypeService, CatalogTypeService>();

            return services;
        }
    }
}
