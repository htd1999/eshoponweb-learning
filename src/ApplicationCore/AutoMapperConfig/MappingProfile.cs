﻿using AutoMapper;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogBrandBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItem;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;

namespace Microsoft.eShopWeb.ApplicationCore.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CatalogItem, CatalogItemBO>();
            CreateMap<CatalogItemBO, CatalogItem>();
            CreateMap<CatalogItem, CreateUpdateCatalogItemRequest>();
            CreateMap<CreateUpdateCatalogItemRequest, CatalogItem>();
            CreateMap<CatalogBrand, ItemWithBrandBO>();
            CreateMap<CatalogType, ItemWithTypeBO>();
            CreateMap<CatalogType, CataLogTypeBO>();
            CreateMap<CatalogBrand, CatalogBrandBO>();
        }
    }
}
