﻿using AutoMapper;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using Microsoft.eShopWeb.ApplicationCore.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.Services
{
    public class CatalogTypeService : ICatalogTypeService
    {
        private readonly IAsyncRepository<CatalogType> _typeRepository;
        private readonly IAppLogger<CatalogTypeService> _logger;
        private readonly IMapper _mapper;

        public CatalogTypeService(IAsyncRepository<CatalogType> typeRepository,
            IAppLogger<CatalogTypeService> logger,
            IMapper mapper)
        {
            _typeRepository = typeRepository;
            _logger = logger;
            _mapper = mapper;
        }
        public async Task<bool> Delete(int Id)
        {
            var type = await _typeRepository.GetByIdAsync(Id);
            if (type != null)
            {
                await _typeRepository.SoftDeleteAsync(type);
                return true;
            }
            return false;
        }

        public async Task<CataLogTypeBO> GetById(int Id)
        {
            var result = await _typeRepository.GetByIdAsync(Id);
            if(result != null && !result.IsDelete)
            {
                return _mapper.Map<CataLogTypeBO>(result);
            }
            return null;
        }

        public async Task<bool> Insert(string typeName)
        {
            try
            {
                var typeSpec = new TypeWithItemsSpecification(typeName);
                var type = await _typeRepository.FirstOrDefaultAsync(typeSpec);
                if (type == null && !String.IsNullOrEmpty(typeName))
                {
                    await _typeRepository.AddAsync(new CatalogType(typeName));
                    return true;
                }
            }
            catch (Exception)
            {
                _logger.LogInformation($"Insert type with Name:{typeName}.");
                throw;
            }
            return false;
        }

        public Task<DefaultGetAllData<CataLogTypeBO>> List(GetTypeRequest request)
        {
            return Task.Run(() =>
            {
                try
                {
                    var types = _typeRepository.ListAllAsync().GetAwaiter().GetResult().AsEnumerable();
                    types = types.Where(_ => _.IsDelete == false && 
                                                _.Type.Trim().ToLower().Contains(request.Type.Trim().ToLower()));
                    int totalItem = 0;
                    if (types != null)
                    {
                        totalItem = types.Count();
                        if (request.IsPaging)
                        {
                            types = types.Skip(request.PageNumber * request.PageLimitItem).Take(request.PageLimitItem);
                        }
                    }
                    return new DefaultGetAllData<CataLogTypeBO>()
                    {
                        Total = totalItem,
                        Data = _mapper.Map<IEnumerable<CataLogTypeBO>>(types.ToList())
                    };
                }
                catch (Exception)
                {
                    throw;
                }
            });

        }

        public async Task<bool> Update(int Id, string typeName)
        {
            try
            {
                if (String.IsNullOrEmpty(typeName))
                {
                    _logger.LogInformation($"Updating type with null type name");
                    return false;
                }
                //check exist
                var type = await _typeRepository.GetByIdAsync(Id);
                if (type == null)
                {
                    _logger.LogInformation($"Updating type with ID:{Id} not found.");
                    return false;
                }

                //check duplicate
                var typeSpec = new TypeWithItemsSpecification(typeName);
                var typeCheckDuplicate = _typeRepository.FirstOrDefaultAsync(typeSpec);
                if(typeCheckDuplicate == null)
                {
                    _logger.LogInformation($"Updating type with ID:{Id} and {typeName} is duplicate.");
                    return false;
                }

                //update execute
                type.UpdateDetail(typeName);
                await _typeRepository.UpdateAsync(type);
                return true;
            }
            catch (Exception)
            {
                _logger.LogInformation($"Updating type with ID:{Id} and {typeName}.");
                return false;
            }
        }
    }
}
