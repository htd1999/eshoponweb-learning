﻿using AutoMapper;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItem;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItemBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using Microsoft.eShopWeb.ApplicationCore.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.Services
{
    public class CatalogItemService : ICatalogItemService
    {
        private readonly IAsyncRepository<CatalogItem> _itemRepository;
        private readonly IAsyncRepository<CatalogType> _typeRepository;
        private readonly IAsyncRepository<CatalogBrand> _brandRepository;
        private readonly IAppLogger<CatalogItemService> _logger;
        private readonly IMapper _mapper;
        public CatalogItemService(
            IAsyncRepository<CatalogItem> itemRepository,
            IAsyncRepository<CatalogType> typeRepository,
            IAsyncRepository<CatalogBrand> brandRepository,
            IAppLogger<CatalogItemService> logger,
            IMapper mapper)
        {
            _itemRepository = itemRepository;
            _typeRepository = typeRepository;
            _brandRepository = brandRepository;
            _logger = logger;
            _mapper = mapper;
        }
        public async Task<bool> Delete(int Id)
        {
            try
            {
                var item = await _itemRepository.GetByIdAsync(Id);
                if (item != null)
                {
                    await _itemRepository.SoftDeleteAsync(item);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<CatalogItemBO> GetById(int Id)
        {
            try
            {
                var result = await _itemRepository.GetByIdAsync(Id);
                if (result != null && !result.IsDelete)
                {
                    return _mapper.Map<CatalogItemBO>(result);
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> Insert(CreateUpdateCatalogItemRequest createItem)
        {
            try
            {
                if(createItem != null)
                {
                    var item = _mapper.Map<CatalogItem>(createItem);
                    await _itemRepository.AddAsync(item);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Inserting item error: {ex.Message}");
                throw;
            }
            return false;
        }

        public Task<DefaultGetAllData<CatalogItemBO>> List(GetItemRequest request)
        {
            return Task.Run(() =>
            {
                try
                {
                    var items = _itemRepository.ListAllAsync().GetAwaiter().GetResult().AsEnumerable();
                    items = items.Where(_ => _.IsDelete == false
                                            && _.Name.Trim().ToLower().Contains(request.Name.Trim().ToLower()));
                    int totalItem = 0;
                    if (items != null)
                    {
                        if (request.CatalogTypeId.HasValue)
                        {
                            items = items.Where(_ => _.CatalogTypeId == request.CatalogTypeId);
                        }
                        if (request.CatalogBrandId.HasValue)
                        {
                            items = items.Where(_ => _.CatalogBrandId == request.CatalogBrandId);
                        }
                        if (request.FromPrice.HasValue)
                        {
                            items = items.Where(_ => _.Price >= request.FromPrice);
                        }
                        if (request.ToPrice.HasValue)
                        {
                            items = items.Where(_ => _.Price <= request.ToPrice);
                        }
                        totalItem = items.Count();
                        if (request.IsPaging)
                        {
                            items = items.Skip(request.PageNumber * request.PageLimitItem).Take(request.PageLimitItem);
                        }
                    }
                    return new DefaultGetAllData<CatalogItemBO>()
                    {
                        Total = totalItem,
                        Data = _mapper.Map<IEnumerable<CatalogItemBO>>(items.ToList())
                    };
                }
                catch (Exception)
                {
                    throw;
                }
            });
        }

        public async Task<bool> Update(int Id, CreateUpdateCatalogItemRequest itemUpdate)
        {
            try
            {
                var itemExist = await _itemRepository.GetByIdAsync(Id);
                if(itemExist != null)
                {
                    itemExist.UpdateDetails(itemUpdate.Name ?? itemExist.Name, 
                                            itemUpdate.Description ?? itemExist.Description, 
                                            itemUpdate.Price ?? itemExist.Price);
                    if(await _typeRepository.FirstOrDefaultAsync(new TypeWithItemsSpecification(itemUpdate.CatalogTypeId ?? Int32.MinValue)) != null)
                    {
                        itemExist.UpdateType(itemUpdate.CatalogTypeId ?? itemExist.CatalogTypeId); 
                    }
                    if (await _brandRepository.FirstOrDefaultAsync(new BrandWithItemsSpecification(itemUpdate.CatalogBrandId ?? Int32.MinValue)) != null)
                    {
                        itemExist.UpdateBrand(itemUpdate.CatalogBrandId ?? itemExist.CatalogBrandId);
                    }
                    itemExist.UpdatePictureUri(itemUpdate.PictureUri);
                    await _itemRepository.UpdateAsync(itemExist);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Updating item with ID:{Id} with error: {ex.Message}");
                throw;
            }
            return false;
        }
    }
}
