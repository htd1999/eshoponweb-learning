﻿using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using Microsoft.eShopWeb.ApplicationCore.Specifications;
using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogBrandBusinessObject;
using AutoMapper;
using System.Collections.Generic;

namespace Microsoft.eShopWeb.ApplicationCore.Services
{
    public class CatalogBrandService : ICatalogBrandService
    {
        private readonly IAsyncRepository<CatalogBrand> _brandRepository;
        private readonly IAppLogger<CatalogBrandService> _logger;
        private readonly IMapper _mapper;

        public CatalogBrandService(IAsyncRepository<CatalogBrand> brandRepository,
            IAppLogger<CatalogBrandService> logger,
            IMapper mapper)
        {
            _brandRepository = brandRepository;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<CatalogBrandBO> GetById(int Id)
        {
            var result = await _brandRepository.GetByIdAsync(Id);
            if(result != null && !result.IsDelete)
            {
                return _mapper.Map<CatalogBrandBO>(result);
            }
            return null;
        }

        public async Task<bool> Insert(string brandName)
        {
            var brandSpec = new BrandWithItemsSpecification(brandName);
            var brand = await _brandRepository.FirstOrDefaultAsync(brandSpec);
            if(brand == null && !String.IsNullOrEmpty(brandName))
            {
                await _brandRepository.AddAsync(new CatalogBrand(brandName));
                return true;
            }
            return false;
        }

        public Task<DefaultGetAllData<CatalogBrandBO>> List(GetBrandRequest request)
        {
            return Task.Run(() =>
            {
                try
                {
                    var brands = _brandRepository.ListAllAsync().GetAwaiter().GetResult().AsEnumerable();
                    brands = brands.Where(_ => _.IsDelete == false && 
                                                _.Brand.Trim().ToLower().Contains(request.Brand.Trim().ToLower()));
                    int totalItem = 0;
                    if (brands != null)
                    {
                        totalItem = brands.Count();
                        if (request.IsPaging)
                        {
                            brands = brands.Skip(request.PageNumber * request.PageLimitItem).Take(request.PageLimitItem);
                        }
                    }
                    return new DefaultGetAllData<CatalogBrandBO>()
                    {
                        Total = totalItem,
                        Data = _mapper.Map<IEnumerable<CatalogBrandBO>>(brands.ToList())
                    };
                }
                catch (Exception)
                {
                    throw;
                }
            });
        }

        public async Task<bool> Delete(int Id)
        {
            var brand = await _brandRepository.GetByIdAsync(Id);
            if(brand != null)
            {
                await _brandRepository.SoftDeleteAsync(brand);
                return true;
            }
            return false;
        }

        public async Task<bool> Update(int Id, string brandName)
        {
            try
            {
                if (String.IsNullOrEmpty(brandName))
                {
                    _logger.LogInformation($"Updating brand with null brand name");
                    return false;
                }
                //check exist
                var brand = await _brandRepository.GetByIdAsync(Id);
                if(brand == null)
                {
                    _logger.LogInformation($"Updating brand with ID:{Id} not found.");
                    return false;
                }

                //check dulicate name
                var brandSpec = new BrandWithItemsSpecification(brandName);
                var brandCheckDuplicate = await _brandRepository.FirstOrDefaultAsync(brandSpec);
                if (brandCheckDuplicate != null)
                {
                    _logger.LogInformation($"Updating brand with ID:{Id} and {brandName} is duplicate.");
                    return false;
                }

                //update execute
                brand.UpdateDetail(brandName);
                await _brandRepository.UpdateAsync(brand);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Updating brand with ID:{Id} and {brandName} with error: {ex.Message}");
                return false;
            }
            
            
        }

    }
}
