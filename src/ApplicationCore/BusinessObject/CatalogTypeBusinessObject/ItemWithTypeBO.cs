﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject
{
    public class ItemWithTypeBO
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
