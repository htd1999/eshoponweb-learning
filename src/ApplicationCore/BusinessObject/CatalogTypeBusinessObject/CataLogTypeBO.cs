﻿using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject
{
    public class CataLogTypeBO
    {
        public int Id { get; set; }
        public string Type { get; private set; }
        public virtual ICollection<CatalogItemBO> Items { get; set; }
    }
}
