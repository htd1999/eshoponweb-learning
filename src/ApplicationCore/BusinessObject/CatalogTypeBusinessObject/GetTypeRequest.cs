﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject
{
    public class GetTypeRequest : DefaultRequest
    {
        public string Type { get; set; } = "";
    }
}
