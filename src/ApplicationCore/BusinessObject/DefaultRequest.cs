﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject
{
    public class DefaultRequest
    {
        public bool IsPaging { get; set; } = false;
        public int PageNumber { get; set; } = 0;
        public int PageLimitItem { get; set; } = 0;
    }
}
