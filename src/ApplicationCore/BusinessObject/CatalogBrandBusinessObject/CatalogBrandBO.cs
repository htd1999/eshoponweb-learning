﻿using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItem;
using System.Collections.Generic;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogBrandBusinessObject
{
    public class CatalogBrandBO
    {
        public int Id { get; set; }
        public string Brand { get; private set; }
        public virtual ICollection<CatalogItemBO> Items { get; set; }
    }
}
