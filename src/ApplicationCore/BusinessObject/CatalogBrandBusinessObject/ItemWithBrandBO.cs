﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogBrandBusinessObject
{
    public class ItemWithBrandBO
    {
        public int Id { get; set; }
        public string Brand { get; set; }
    }
}
