﻿using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogBrandBusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItemBusinessObject
{
    public class TypeWithItemBO
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public decimal Price { get; private set; }
        public string PictureUri { get; private set; }
        public ItemWithBrandBO CatalogBrand { get; private set; }
    }
}
