﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItemBusinessObject
{
    public class GetItemRequest : DefaultRequest
    {
        public string Name { get; set; } = "";

        public decimal? FromPrice { get; set; }
        public decimal? ToPrice { get; set; }

        public int? CatalogTypeId { get; set; }
        public int? CatalogBrandId { get; set; }
    }
}
