﻿using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogBrandBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItem
{
    public class CatalogItemBO
    {
        public int Id { get; set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public decimal Price { get; private set; }
        public string PictureUri { get; private set; }
        public ItemWithTypeBO CatalogType { get; private set; }
        public ItemWithBrandBO CatalogBrand { get; private set; }
    }
}
