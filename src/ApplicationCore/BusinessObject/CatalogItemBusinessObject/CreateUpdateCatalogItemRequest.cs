﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItem
{
    public class CreateUpdateCatalogItemRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public string PictureUri { get; set; }
        public int? CatalogTypeId { get; set; }
        public int? CatalogBrandId { get; set; }
    }
}
