﻿using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItem;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogItemBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.Interfaces
{
    public interface ICatalogItemService
    {
        Task<DefaultGetAllData<CatalogItemBO>> List(GetItemRequest request);
        Task<CatalogItemBO> GetById(int Id);
        Task<bool> Insert(CreateUpdateCatalogItemRequest item);
        Task<bool> Update(int Id, CreateUpdateCatalogItemRequest item);
        Task<bool> Delete(int Id);
    }
}
