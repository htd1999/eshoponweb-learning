﻿using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogTypeBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.Interfaces
{
    public interface ICatalogTypeService
    {
        Task<DefaultGetAllData<CataLogTypeBO>> List(GetTypeRequest request);
        Task<CataLogTypeBO> GetById(int Id);
        Task<bool> Insert(string brandName);
        Task<bool> Update(int Id, string brandName);
        Task<bool> Delete(int Id);
    }
}
