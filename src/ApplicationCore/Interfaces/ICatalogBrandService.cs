﻿using Microsoft.eShopWeb.ApplicationCore.BusinessObject;
using Microsoft.eShopWeb.ApplicationCore.BusinessObject.CatalogBrandBusinessObject;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.Interfaces
{
    public interface ICatalogBrandService
    {
        Task<DefaultGetAllData<CatalogBrandBO>> List(GetBrandRequest request);
        Task<CatalogBrandBO> GetById(int Id);
        Task<bool> Insert(string brandName);
        Task<bool> Update(int Id, string brandName);
        Task<bool> Delete(int Id);
    }
}
