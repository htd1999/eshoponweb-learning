﻿using Ardalis.Specification;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.Specifications
{
    public class BrandWithItemsSpecification : Specification<CatalogBrand>
    {
        public BrandWithItemsSpecification()
        {
            Query.Include(_ => _.Items).ThenInclude(_ => _.CatalogType);
        }
        public void IncludeItem()
        {
            Query.Include(_ => _.Items).ThenInclude(_ => _.CatalogType);
        }
        public BrandWithItemsSpecification(string brand)
        {
            Query
                .Where(b => b.Brand == brand);
        }

        public BrandWithItemsSpecification(int Id)
        {
            Query
                .Where(b => b.Id == Id);
        }
    }
}
