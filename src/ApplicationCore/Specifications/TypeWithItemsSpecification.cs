﻿using Ardalis.Specification;
using Microsoft.eShopWeb.ApplicationCore.Entities;

namespace Microsoft.eShopWeb.ApplicationCore.Specifications
{
    public class TypeWithItemsSpecification : Specification<CatalogType>
    {
        public TypeWithItemsSpecification(string type)
        {
            Query
                .Where(b => b.Type == type);
        }
        public TypeWithItemsSpecification(int Id)
        {
            Query
                .Where(b => b.Id == Id);
        }
    }
}
