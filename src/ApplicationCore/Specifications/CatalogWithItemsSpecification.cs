﻿using Ardalis.Specification;
using Microsoft.eShopWeb.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.eShopWeb.ApplicationCore.Specifications
{
    public class CatalogWithItemsSpecification : Specification<CatalogItem>
    {
        public CatalogWithItemsSpecification(string name)
        {
            Query
                .Where(b => b.Name == name);
        }

        public CatalogWithItemsSpecification(int Id)
        {
            Query
                .Where(b => b.Id == Id);
        }
    }
}
