﻿using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using System.Collections.Generic;

namespace Microsoft.eShopWeb.ApplicationCore.Entities
{
    public class CatalogBrand : BaseEntity, IAggregateRoot
    {
        public string Brand { get; private set; }
        public virtual ICollection<CatalogItem> Items { get; private set; }
        public CatalogBrand(string brand)
        {
            Brand = brand;
        }

        public CatalogBrand(int id, string brand)
        {
            Id = id;
            Brand = brand;
        }

        public CatalogBrand()
        {
            Items = new HashSet<CatalogItem>();
        }

        public void UpdateDetail(string brand)
        {
            if(!string.IsNullOrEmpty(brand) && !string.IsNullOrWhiteSpace(brand))
            {
                Brand = brand;
            }
        }
    }
}
