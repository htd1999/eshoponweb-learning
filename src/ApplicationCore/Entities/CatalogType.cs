﻿using Microsoft.eShopWeb.ApplicationCore.Interfaces;
using System.Collections.Generic;

namespace Microsoft.eShopWeb.ApplicationCore.Entities
{
    public class CatalogType : BaseEntity, IAggregateRoot
    {
        public string Type { get; private set; }
        public virtual ICollection<CatalogItem> Items { get; private set; }
        public CatalogType(string type)
        {
            Type = type;
        }

        public CatalogType(int id, string type)
        {
            Id = id;
            Type = type;
        }

        public CatalogType()
        {
            Items = new HashSet<CatalogItem>();
        }

        public void UpdateDetail(string type)
        {
            if (!string.IsNullOrEmpty(type) && !string.IsNullOrWhiteSpace(type))
            {
                Type = type;
            }
        }
    }
}
